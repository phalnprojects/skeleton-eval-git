<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Projet</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	<?php include_once 'inc/header.php'; ?>

        <main>
	    <article>
		<header>
		    <h1>Welcome!</h1>
		</header>
		<p>Démonstration de l'architecture des fichiers d'une application web.</p>
                <p>Consultez le fichier README.MD pour plus d'informations.</p>
	    </article>
        </main>

	<?php include_once 'inc/footer.php'; ?>
    </body>
</html>

